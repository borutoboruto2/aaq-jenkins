package fr.aaq.aaqcd

import com.lesfurets.jenkins.unit.BasePipelineTest
import org.junit.Before
import org.junit.Test;

/**
 * @author sareaboudousamadou.
 */
class JobTest extends BasePipelineTest{

    @Override
    @Before
    public void setUp() throws Exception {

        scriptRoots += 'src/main/groovy/fr/aaq/aaqcd'
        super.setUp();
    }

    @Test
    void should_execute_without_errors() {
        def script = loadScript("test-job.pipeline")
        script.execute()
        printCallStack()
    }
}
